<?php

namespace Drupal\entity_field_lookup\Plugin\migrate\process;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\Annotation\MigrateProcessPlugin;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Plugin\Exception\BadPluginDefinitionException;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * This plugin looks for an existing entity using a field value.
 *
 * Example usage with full configuration:
 *
 * @code
 *   field_tags:
 *     plugin: entity_field_lookup
 *     source: source_key
 *     entity_type_id: node
 *     bundle_key: type
 *     bundle_id: article
 *     entity_field: field_name
 *     access_check: false
 *     extra_conditions:
 *       - field: field_number
 *         value: 3
 *         operator: '>='
 *         langcode: en
 *       - field: field_string
 *         value: 'something'
 *         operator: NULL
 *         langcode: NULL
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "entity_field_lookup",
 *   handle_multiples = FALSE
 * )
 */
class EntityFieldLookup extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * @var EntityTypeManagerInterface
   */
  private EntityTypeManagerInterface $entityTypeManager;

  /**
   * The access check flag.
   *
   * @var bool
   */
  private bool $accessCheck = TRUE;

  /**
   * {@inheritdoc}
   *
   * @throws BadPluginDefinitionException
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;

    $ensure_conf = [
      'entity_type_id',
      'bundle_key',
      'bundle_id',
      'entity_field',
    ];
    foreach ($ensure_conf as $item) {
      $conf = $this->configuration[$item];
      if (empty($conf)) {
        throw new BadPluginDefinitionException('entity_field_lookup', $item);
      }
    }
    $entity_type_id = $this->configuration['entity_type_id'];
    if (!$this->entityTypeManager->getDefinition($entity_type_id)) {
      throw new InvalidPluginDefinitionException("Plugin entity_field_lookup was configured to use non existing entity type {$entity_type_id}.");
    }

    if (!empty($this->configuration["extra_conditions"])) {
      foreach ($this->configuration["extra_conditions"] as $delta => $extra_condition) {
        if (empty($extra_condition["field"])) {
          throw new InvalidPluginDefinitionException("The extra condition {$delta} of the entity_field_lookup needs a non-NULL 'field' configuration.");
        }
      }
    }

    if (isset($this->configuration['access_check'])) {
      $this->accessCheck = $this->configuration['access_check'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (is_null($value)) {
      return NULL;
    }
    $entity_type_id = $this->configuration["entity_type_id"];
    $bundle_key = $this->configuration["bundle_key"];
    $bundle_id = $this->configuration["bundle_id"];
    if (!is_array($bundle_id)) {
      $bundle_id = [$bundle_id];
    }
    $entity_field = $this->configuration["entity_field"];

    $storage = $this->entityTypeManager->getStorage($entity_type_id);
    $query = $storage
      ->getQuery()
      ->accessCheck($this->accessCheck)
      ->condition($bundle_key, $bundle_id, 'IN')
      ->condition($entity_field, $value);

    if (!empty($this->configuration["extra_conditions"])) {
      foreach ($this->configuration["extra_conditions"] as $delta => $extra_condition) {
        $field = $extra_condition["field"];
        $value = $extra_condition["value"] ?? NULL;
        $operator = $extra_condition["operator"] ?? NULL;
        $langcode = $extra_condition["langcode"] ?? NULL;
        $query->condition($field, $value, $operator, $langcode);
      }
    }

    $results = $query->execute();

    if (empty($results)) {
      return NULL;
    }

    return reset($results);
  }

}
